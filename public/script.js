let overlayNav = document.getElementById("overlay-nav");
let navIcon = document.getElementById("nav-icon");
let closeProject = document.getElementById("close-project");

function open() {
  overlayNav.style.visibility = "visible";
  overlayNav.style.opacity = "1";
}

function close() {
  overlayNav.style.visibility = "hidden";
  overlayNav.style.opacity = "0";
}

navIcon && navIcon.addEventListener("click", function () {
  if (this.classList.contains("open")) close();
  else open();
  this.classList.toggle("open");
});

closeProject && closeProject.addEventListener("click", function () {
  window.history.back();
});